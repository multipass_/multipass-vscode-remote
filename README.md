# Multipass Ansible Toolkit

[![Software License](https://img.shields.io/badge/license-GPL%20v3-informational.svg?style=flat&logo=gnu)](LICENSE)

An simple and auto-updated [Multipass](https://multipass.run/) VM with docker inside, ready to use for [VSCode Remote](https://code.visualstudio.com/docs/remote/remote-overview).

## Getting Started

### Prerequisities

In order to deploy your environment:

* [Multipass](https://multipass.run/)
* [Task](https://taskfile.dev)
* curl

### Installation

#### Initialization

* Clone the repository:

```shell
git clone https://gitlab.com/multipass_/multipass-vscode-remote.git
```

* Download resources:

```shell
cd multipass-vscode-remote
task download
```

* [Optional] Edit the configuration file `multipass.cfg` in `files` directory.

#### Deploy

Only one command:

```shell
task provision
```

List of task commands are available with the command `task` without argument:

```shell
$ task
task: Available tasks for this project:
* download: 		[CORE] Download necessary resources.
* mp:create: 		[MULTIPASS] Create instances.
* mp:del: 		[MULTIPASS] Delete instances.
* mp:list: 		[MULTIPASS] List instances.
* mp:start: 		[MULTIPASS] Start instances.
* mp:stop: 		[MULTIPASS] Stop instances.
* provision: 		[CORE] Deploy a VM with docker.
* sys:generate-keys: 	[SYSTEM] generate keys for all instances.
* sys:ssh: 		[SYSTEM] ssh connection. Arguments: HOST=remote-vsc
```

## Authors

* **FX Soubirou** - *Initial work* - [Gitlab repositories](https://gitlab.com/users/fxs/projects)

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. See the [LICENSE](https://gitlab.com/fxs/vagrant-virtualbox-ansible-k3s/blob/master/LICENSE) file for details.
